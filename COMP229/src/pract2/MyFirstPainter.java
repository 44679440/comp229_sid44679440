package pract2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Cell {
	int x, y, width, height;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Cell(int x, int y, int width, int height) {
		super();
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}

	public boolean isPointIncell(int px, int py) {
		return false;

	}
}

public class MyFirstPainter extends JPanel {
	static Cell[][] myCells = new Cell[20][20];

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.setStroke(new BasicStroke(2));
		g2.setColor(Color.red);

		int cellsize = 35;
		int gap = 10;
		for (int i = 0; i < 20; i++) {
			for (int k = 0; k < 20; k++) {
				drawCell(myCells[i][k], g2);
				
			}
		}
	
	}

	private void checkCell(Cell cell) {
		// TODO Auto-generated method stub
		for (int i = 0; i < 20; i++) {
			for (int k = 0; k < 20; k++) {

			}
		}

	}

	private void drawCell(Cell cell, Graphics2D g2) {
		int gap = 10;
		g2.drawRect(gap + cell.x * cell.width, gap + cell.y * cell.height, cell.height, cell.width);

	}

	public static void main(String[] args) {

		int cellsize = 35;

		for (int i = 0; i < 20; i++) {
			for (int k = 0; k < 20; k++) {

				// myCells[i][k] = new Cell(gap+cellsize*i, gap+cellsize*k,
				// cellsize, cellsize);
				myCells[i][k] = new Cell(i, k, cellsize, cellsize);
			}
		}

		JFrame frame = new JFrame(" ");
		// frame.setSize(1280, 720); // window size

		frame.setResizable(false);
		MyFirstPainter p = new MyFirstPainter();
		p.setPreferredSize(new Dimension(1280, 720));
		frame.add(p);
		frame.pack();

		frame.setVisible(true);
	}

}